-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 09, 2019 at 04:49 PM
-- Server version: 10.1.40-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baleit_sidamping`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota_fasilitator`
--

CREATE TABLE `anggota_fasilitator` (
  `id_af` int(11) NOT NULL,
  `id_timfasilitator` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `anggota_pokmas`
--

CREATE TABLE `anggota_pokmas` (
  `id_ap` int(11) NOT NULL,
  `nama_ap` varchar(128) DEFAULT NULL,
  `nik_ap` varchar(128) DEFAULT NULL,
  `kk_ap` varchar(128) DEFAULT NULL,
  `skd_pds` varchar(256) DEFAULT NULL,
  `status_ap` int(11) DEFAULT NULL,
  `alamat_ap` varchar(256) DEFAULT NULL,
  `rt_ap` varchar(8) DEFAULT NULL,
  `rw_ap` varchar(8) DEFAULT NULL,
  `dusun_ap` varchar(64) DEFAULT NULL,
  `kelurahan_ap` varchar(64) DEFAULT NULL,
  `kecamatan` varchar(64) DEFAULT NULL,
  `kabupaten` varchar(64) DEFAULT NULL,
  `kode_wil` varchar(8) DEFAULT NULL,
  `id_pokmas` int(11) NOT NULL,
  `no_rek` varchar(64) DEFAULT NULL,
  `nama_bank` int(64) DEFAULT NULL,
  `rek_terisi` double DEFAULT NULL,
  `kategori_rusak` int(11) DEFAULT NULL,
  `sumber_dana` int(11) DEFAULT NULL,
  `status_lahan` int(11) DEFAULT NULL,
  `luas_lahan` double DEFAULT NULL,
  `luas_bangun` double DEFAULT NULL,
  `file_gbr_rencana_tekniks` varchar(256) NOT NULL,
  `file_rab` varchar(256) NOT NULL,
  `file_rpd` varchar(256) NOT NULL,
  `file_ppk_bpdb` varchar(256) NOT NULL,
  `file_perencanaan` varchar(256) NOT NULL,
  `file_spk` varchar(256) NOT NULL,
  `tgl_mulai_spk` date NOT NULL,
  `tgl_selesai_spk` date NOT NULL,
  `pencairan_1` double NOT NULL,
  `pencairan_2` double NOT NULL,
  `pencairan_3` double NOT NULL,
  `bowplank` int(11) DEFAULT NULL,
  `pondasi` int(11) DEFAULT NULL,
  `sloof` int(11) DEFAULT NULL,
  `kolom` int(11) DEFAULT NULL,
  `ringblok` int(11) DEFAULT NULL,
  `atap` int(11) DEFAULT NULL,
  `dinding` int(11) DEFAULT NULL,
  `lantai` int(11) DEFAULT NULL,
  `kusen` int(11) DEFAULT NULL,
  `perbaikan_50` int(11) DEFAULT NULL,
  `dok_perbaikan_50` varchar(256) DEFAULT NULL,
  `perbaikan_100` int(11) DEFAULT NULL,
  `dok_perbaikan_100` varchar(256) DEFAULT NULL,
  `dok_lpj` varchar(256) DEFAULT NULL,
  `tgl_upload_lpj` date DEFAULT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cf_values`
--

CREATE TABLE `cf_values` (
  `cf_values_id` int(11) UNSIGNED NOT NULL,
  `rel_crud_id` int(11) DEFAULT NULL,
  `cf_id` int(11) DEFAULT NULL,
  `curd` varchar(250) DEFAULT NULL,
  `value` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cf_values`
--

INSERT INTO `cf_values` (`cf_values_id`, `rel_crud_id`, `cf_id`, `curd`, `value`) VALUES
(1, 1, 1, 'user', '08175731377'),
(2, 2, 1, 'user', ''),
(3, 3, 1, 'user', '08175731377');

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `custom_fields_id` int(11) UNSIGNED NOT NULL,
  `rel_crud` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `options` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `show_in_grid` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`custom_fields_id`, `rel_crud`, `name`, `type`, `required`, `options`, `status`, `show_in_grid`, `create_date`) VALUES
(1, 'user', 'Telp', 'numbers', 0, NULL, 'active', 1, '2019-07-09 08:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `gbr_progress`
--

CREATE TABLE `gbr_progress` (
  `id_gbr_progress` int(11) NOT NULL,
  `id_progress` int(11) NOT NULL,
  `file_foto_progress` varchar(256) NOT NULL,
  `keterangan_gbr` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(122) UNSIGNED NOT NULL,
  `user_type` varchar(250) DEFAULT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `user_type`, `data`) VALUES
(1, 'admin', '{\"user\":\"user\"}'),
(2, 'Administrator', '{\"user\":\"user\"}'),
(3, 'Fasilitator', '{\"user\":\"user\"}'),
(4, 'Ketua Pelaksana', '{\"user\":\"user\"}'),
(5, 'Anggota Pokmas', '{\"user\":\"user\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pokmas`
--

CREATE TABLE `pokmas` (
  `id_pokmas` int(11) NOT NULL,
  `nama_pokmas` varchar(256) NOT NULL,
  `sk_pokmas` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `progress_perbaikan`
--

CREATE TABLE `progress_perbaikan` (
  `id_progress_perbaikan` int(11) NOT NULL,
  `id_ap` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tgl_submit` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(122) UNSIGNED NOT NULL,
  `keys` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `keys`, `value`) VALUES
(1, 'website', 'SIDAMPING'),
(2, 'logo', NULL),
(3, 'favicon', NULL),
(4, 'SMTP_EMAIL', 'sidamping@bale-it.co.id'),
(5, 'HOST', 'mail.bale-it.co.id'),
(6, 'PORT', '465'),
(7, 'SMTP_SECURE', ''),
(8, 'SMTP_PASSWORD', 'sidamping1234'),
(9, 'mail_setting', 'php_mailer'),
(10, 'company_name', 'Bale Informasi Teknologi'),
(11, 'crud_list', 'User'),
(12, 'EMAIL', ''),
(13, 'UserModules', 'yes'),
(14, 'register_allowed', '1'),
(15, 'email_invitation', '1'),
(16, 'admin_approval', '0'),
(17, 'language', 'english'),
(18, 'user_type', '[\"Fasilitator\",\"Anggota Pokmas\"]');

-- --------------------------------------------------------

--
-- Table structure for table `status_anggota`
--

CREATE TABLE `status_anggota` (
  `id_statusanggota` int(11) NOT NULL,
  `nama_statusanggota` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_lahan`
--

CREATE TABLE `status_lahan` (
  `id_statuslahan` int(11) NOT NULL,
  `nama_statuslahan` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sumber_dana`
--

CREATE TABLE `sumber_dana` (
  `id_sumberdana` int(11) NOT NULL,
  `nama_sumberdana` varchar(256) NOT NULL,
  `ket_sumberdana` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `id` int(121) UNSIGNED NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `html` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `module`, `code`, `template_name`, `html`) VALUES
(1, 'forgot_pass', 'forgot_password', 'Forgot password', '<html xmlns=\"http://www.w3.org/1999/xhtml\"><head>\r\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n  <style type=\"text/css\" rel=\"stylesheet\" media=\"all\">\r\n    /* Base ------------------------------ */\r\n    *:not(br):not(tr):not(html) {\r\n      font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;\r\n      -webkit-box-sizing: border-box;\r\n      box-sizing: border-box;\r\n    }\r\n    body {\r\n      \r\n    }\r\n    a {\r\n      color: #3869D4;\r\n    }\r\n\r\n\r\n    /* Masthead ----------------------- */\r\n    .email-masthead {\r\n      padding: 25px 0;\r\n      text-align: center;\r\n    }\r\n    .email-masthead_logo {\r\n      max-width: 400px;\r\n      border: 0;\r\n    }\r\n    .email-footer {\r\n      width: 570px;\r\n      margin: 0 auto;\r\n      padding: 0;\r\n      text-align: center;\r\n    }\r\n    .email-footer p {\r\n      color: #AEAEAE;\r\n    }\r\n  \r\n    .content-cell {\r\n      padding: 35px;\r\n    }\r\n    .align-right {\r\n      text-align: right;\r\n    }\r\n\r\n    /* Type ------------------------------ */\r\n    h1 {\r\n      margin-top: 0;\r\n      color: #2F3133;\r\n      font-size: 19px;\r\n      font-weight: bold;\r\n      text-align: left;\r\n    }\r\n    h2 {\r\n      margin-top: 0;\r\n      color: #2F3133;\r\n      font-size: 16px;\r\n      font-weight: bold;\r\n      text-align: left;\r\n    }\r\n    h3 {\r\n      margin-top: 0;\r\n      color: #2F3133;\r\n      font-size: 14px;\r\n      font-weight: bold;\r\n      text-align: left;\r\n    }\r\n    p {\r\n      margin-top: 0;\r\n      color: #74787E;\r\n      font-size: 16px;\r\n      line-height: 1.5em;\r\n      text-align: left;\r\n    }\r\n    p.sub {\r\n      font-size: 12px;\r\n    }\r\n    p.center {\r\n      text-align: center;\r\n    }\r\n\r\n    /* Buttons ------------------------------ */\r\n    .button {\r\n      display: inline-block;\r\n      width: 200px;\r\n      background-color: #3869D4;\r\n      border-radius: 3px;\r\n      color: #ffffff;\r\n      font-size: 15px;\r\n      line-height: 45px;\r\n      text-align: center;\r\n      text-decoration: none;\r\n      -webkit-text-size-adjust: none;\r\n      mso-hide: all;\r\n    }\r\n    .button--green {\r\n      background-color: #22BC66;\r\n    }\r\n    .button--red {\r\n      background-color: #dc4d2f;\r\n    }\r\n    .button--blue {\r\n      background-color: #3869D4;\r\n    }\r\n  </style>\r\n</head>\r\n<body style=\"width: 100% !important;\r\n      height: 100%;\r\n      margin: 0;\r\n      line-height: 1.4;\r\n      background-color: #F2F4F6;\r\n      color: #74787E;\r\n      -webkit-text-size-adjust: none;\">\r\n  <table class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"\r\n    width: 100%;\r\n    margin: 0;\r\n    padding: 0;\">\r\n    <tbody><tr>\r\n      <td align=\"center\">\r\n        <table class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\r\n      margin: 0;\r\n      padding: 0;\">\r\n          <!-- Logo -->\r\n\r\n          <tbody>\r\n          <!-- Email Body -->\r\n          <tr>\r\n            <td class=\"email-body\" width=\"100%\" style=\"width: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n    border-top: 1px solid #edeef2;\r\n    border-bottom: 1px solid #edeef2;\r\n    background-color: #edeef2;\">\r\n              <table class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\" style=\" width: 570px;\r\n    margin:  14px auto;\r\n    background: #fff;\r\n    padding: 0;\r\n    border: 1px outset rgba(136, 131, 131, 0.26);\r\n    box-shadow: 0px 6px 38px rgb(0, 0, 0);\r\n       \">\r\n                <!-- Body content -->\r\n                <thead style=\"background: #3869d4;\"><tr><th><div align=\"center\" style=\"padding: 15px; color: #000;\"><a href=\"{var_action_url}\" class=\"email-masthead_name\" style=\"font-size: 16px;\r\n      font-weight: bold;\r\n      color: #bbbfc3;\r\n      text-decoration: none;\r\n      text-shadow: 0 1px 0 white;\">{var_sender_name}</a></div></th></tr>\r\n                </thead>\r\n                <tbody><tr>\r\n                  <td class=\"content-cell\" style=\"padding: 35px;\">\r\n                    <h1>Hi {var_user_name},</h1>\r\n                    <p>You recently requested to reset your password for your {var_website_name} account. Click the button below to reset it.</p>\r\n                    <!-- Action -->\r\n                    <table class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"\r\n      width: 100%;\r\n      margin: 30px auto;\r\n      padding: 0;\r\n      text-align: center;\">\r\n                      <tbody><tr>\r\n                        <td align=\"center\">\r\n                          <div>\r\n                            <!--[if mso]><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"{{var_action_url}}\" style=\"height:45px;v-text-anchor:middle;width:200px;\" arcsize=\"7%\" stroke=\"f\" fill=\"t\">\r\n                              <v:fill type=\"tile\" color=\"#dc4d2f\" />\r\n                              <w:anchorlock/>\r\n                              <center style=\"color:#ffffff;font-family:sans-serif;font-size:15px;\">Reset your password</center>\r\n                            </v:roundrect><![endif]-->\r\n                            <a href=\"{var_varification_link}\" class=\"button button--red\" style=\"background-color: #dc4d2f;display: inline-block;\r\n      width: 200px;\r\n      background-color: #3869D4;\r\n      border-radius: 3px;\r\n      color: #ffffff;\r\n      font-size: 15px;\r\n      line-height: 45px;\r\n      text-align: center;\r\n      text-decoration: none;\r\n      -webkit-text-size-adjust: none;\r\n      mso-hide: all;\">Reset your password</a>\r\n                          </div>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody></table>\r\n                    <p>If you did not request a password reset, please ignore this email or reply to let us know.</p>\r\n                    <p>Thanks,<br>{var_sender_name} and the {var_website_name} Team</p>\r\n                   <!-- Sub copy -->\r\n                    <table class=\"body-sub\" style=\"margin-top: 25px;\r\n      padding-top: 25px;\r\n      border-top: 1px solid #EDEFF2;\">\r\n                      <tbody><tr>\r\n                        <td> \r\n                          <p class=\"sub\" style=\"font-size:12px;\">If you are having trouble clicking the password reset button, copy and paste the URL below into your web browser.</p>\r\n                          <p class=\"sub\"  style=\"font-size:12px;\"><a href=\"{var_varification_link}\">{var_varification_link}</a></p>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody></table>\r\n                  </td>\r\n                </tr>\r\n              </tbody></table>\r\n            </td>\r\n          </tr>\r\n        </tbody></table>\r\n      </td>\r\n    </tr>\r\n  </tbody></table>\r\n\r\n\r\n</body></html>'),
(2, 'users', 'invitation', 'Invitation', '<p>Hello <strong>{var_user_email}</strong></p>\r\n\r\n<p>Click below link to register&nbsp;<br />\r\n{var_inviation_link}</p>\r\n\r\n<p>Thanks&nbsp;</p>\r\n'),
(3, 'registration', 'registration', 'Registration', '<p>Hello <strong>{var_user_name}</strong></p>\r\n<p>Welcome to Notes&nbsp;<br />\r\n<p>To complete your registration&nbsp;<br /><br />\r\n<a href=\"{var_varification_link}\">please click here</a></p>\r\n\n<p>Thanks&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tim_fasilitastor`
--

CREATE TABLE `tim_fasilitastor` (
  `id_tim_fasilitator` int(11) NOT NULL,
  `nama_tim_fasilitator` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(121) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `var_key` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `is_deleted` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `create_date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `user_id`, `var_key`, `status`, `is_deleted`, `name`, `password`, `email`, `profile_pic`, `user_type`, `create_date`) VALUES
(1, '1', '$2y$10$MmuUC86SkaKe0aa2uj5XAOvJpq36vBgdkDzVmmueA70qtLYpf1ptq', 'active', '0', 'Superadmin', '$2y$10$y8ApYEgBqOe0O3.L1q.iHOGRcm7nbT3KuNTpyI1GNfdGt4WvubfKe', 'baleinformasiteknologi@gmail.com', 'widiarta - 11_1562653925.JPG', 'admin', '2017-08-18'),
(2, '1', NULL, 'active', '0', 'Gusti Ngurah M', '$2y$10$AAZFULxMBna46FVT7CDUcuWV0FifhHhIkP1ANzpE49SipM0VHf6UW', 'gusti.ngurah.mertayasa@gmail.com', 'user.png', 'Fasilitator', '2019-07-09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota_fasilitator`
--
ALTER TABLE `anggota_fasilitator`
  ADD PRIMARY KEY (`id_af`);

--
-- Indexes for table `anggota_pokmas`
--
ALTER TABLE `anggota_pokmas`
  ADD PRIMARY KEY (`id_ap`),
  ADD KEY `id_pokmas` (`id_pokmas`);

--
-- Indexes for table `cf_values`
--
ALTER TABLE `cf_values`
  ADD PRIMARY KEY (`cf_values_id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`custom_fields_id`);

--
-- Indexes for table `gbr_progress`
--
ALTER TABLE `gbr_progress`
  ADD PRIMARY KEY (`id_gbr_progress`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pokmas`
--
ALTER TABLE `pokmas`
  ADD PRIMARY KEY (`id_pokmas`),
  ADD KEY `id_pokmas` (`id_pokmas`),
  ADD KEY `id_pokmas_2` (`id_pokmas`);

--
-- Indexes for table `progress_perbaikan`
--
ALTER TABLE `progress_perbaikan`
  ADD PRIMARY KEY (`id_progress_perbaikan`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_anggota`
--
ALTER TABLE `status_anggota`
  ADD PRIMARY KEY (`id_statusanggota`);

--
-- Indexes for table `status_lahan`
--
ALTER TABLE `status_lahan`
  ADD PRIMARY KEY (`id_statuslahan`);

--
-- Indexes for table `sumber_dana`
--
ALTER TABLE `sumber_dana`
  ADD PRIMARY KEY (`id_sumberdana`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tim_fasilitastor`
--
ALTER TABLE `tim_fasilitastor`
  ADD PRIMARY KEY (`id_tim_fasilitator`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota_fasilitator`
--
ALTER TABLE `anggota_fasilitator`
  MODIFY `id_af` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `anggota_pokmas`
--
ALTER TABLE `anggota_pokmas`
  MODIFY `id_ap` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cf_values`
--
ALTER TABLE `cf_values`
  MODIFY `cf_values_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `custom_fields_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gbr_progress`
--
ALTER TABLE `gbr_progress`
  MODIFY `id_gbr_progress` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(122) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `progress_perbaikan`
--
ALTER TABLE `progress_perbaikan`
  MODIFY `id_progress_perbaikan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(122) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `status_anggota`
--
ALTER TABLE `status_anggota`
  MODIFY `id_statusanggota` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_lahan`
--
ALTER TABLE `status_lahan`
  MODIFY `id_statuslahan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sumber_dana`
--
ALTER TABLE `sumber_dana`
  MODIFY `id_sumberdana` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(121) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tim_fasilitastor`
--
ALTER TABLE `tim_fasilitastor`
  MODIFY `id_tim_fasilitator` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anggota_pokmas`
--
ALTER TABLE `anggota_pokmas`
  ADD CONSTRAINT `anggota_pokmas_ibfk_1` FOREIGN KEY (`id_pokmas`) REFERENCES `pokmas` (`id_pokmas`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
